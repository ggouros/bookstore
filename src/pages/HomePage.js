import React from 'react';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    mainGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    }
}));

export default function SingleProduct() {
    const classes = useStyles();

    return (
        <React.Fragment>
            <Header />
            <main>
                <Container className={classes.mainGrid} maxWidth="md">
                    <Grid container spacing={2} justify="center">
                        <Grid item xs={12} sm={7} md={7} >
                            <Typography component="h5" variant="h5" paragraph align="center">
                                {`Author: George Gouros`}
                            </Typography>
                        </Grid>
                        <Grid item xs={12} sm={7} md={7} >
                            <Typography component="h5" variant="h5" paragraph align="center">
                                Libraries: 
                            </Typography>
                            <Typography paragraph align="center">
                                <Link href="https://reactjs.org/">
                                    React
                                </Link>
                            </Typography>
                            <Typography paragraph align="center">
                                <Link href="https://github.com/ReactTraining/react-router">
                                    React Router
                                </Link>
                            </Typography>
                            <Typography paragraph align="center">
                                <Link href="https://material-ui.com/">
                                    Material-UI
                                </Link>
                            </Typography>
                            <Typography paragraph align="center">
                                <Link href="https://github.com/axios/axios">
                                    Axios
                                </Link>
                            </Typography>
                            <Typography paragraph align="center">
                                <Link href="https://github.com/akiran/react-slick">
                                    Slick
                                </Link>
                            </Typography>
                        </Grid>
                    </Grid>
                </Container>
            </main>
            <Footer />
        </React.Fragment>
    );
}
