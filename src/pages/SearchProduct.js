import React, { useContext, useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';

import { BooksContext } from '../contexts/books';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';
import SingleProduct from '../components/SingleProduct/SingleProduct';
import Loading from '../components/Loading/Loading';

import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import Chip from '@material-ui/core/Chip';

const useStyles = makeStyles(theme => ({
    icon: {
        marginRight: theme.spacing(2),
    },
    heroContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    },
    heroButtons: {
        marginTop: theme.spacing(4),
    },
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    searchBox: {
        border: 'groove',
        paddingLeft: '10px',
    },


    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        maxWidth: 300,
        align: 'center'
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: 2,
    },
    noLabel: {
        marginTop: theme.spacing(3),
    },
    filters: {
        justifyContent: 'center',
        display: 'flex',
        flexWrap: 'wrap',

    }
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

export default function SearchProduct() {
    const [searchTxt, setSearchTxt] = useState('');
    const classes = useStyles();

    const [bookFilters, setBookFilters] = useState({
        category: "",
        year: "",
        publisher: ""
    });

    let allFilters = {
        category: [],
        year: [],
        publisher: []
    };

    const { getBooks, books } = useContext(BooksContext);
    // Similar to componentDidMount and componentDidUpdate:
    useEffect(() => {
        // Get books
        if (!books.data || (books.data.length < 5)) {
            getBooks();
        }
    }, []);


    books.data && books.data.map(book => {
        book.category.map(category => {
            if (allFilters.category.indexOf(category) === -1) {
                allFilters.category.push(category);
            }
        })
        if (allFilters.year.indexOf(book.published.substring(0, 4)) === -1) {
            allFilters.year.push(book.published.substring(0, 4));
        }
        if (allFilters.publisher.indexOf(book.publisher) === -1) {
            allFilters.publisher.push(book.publisher);
        }
    })

    const handleChangeFilter = (event, type) => {
        setBookFilters({
            ...bookFilters,
            [type]: event.target.value
        });
    };

    return (
        !books.data || books.loading
            ?
            <Loading />
            :
            <React.Fragment>
                <Header />
                <main>
                    {/* Hero unit */}
                    <div className={classes.heroContent}>
                        <Container maxWidth="sm">
                            <Typography component="h3" variant="h4" align="center" color="textPrimary" gutterBottom>
                                Search to find your new book
                            </Typography>
                            <Typography align="center" color="textSecondary" paragraph>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
                            </Typography>
                            <div className={classes.heroButtons}>
                                <Grid container spacing={2} justify="center">
                                    <div className={classes.searchBox}>
                                        <InputBase
                                            className={classes.input}
                                            placeholder="Search product"
                                            inputProps={{ 'aria-label': 'search product' }}
                                            onChange={(event) => {
                                                setSearchTxt(event.target.value);
                                            }}
                                        />
                                        <IconButton type="submit" className={classes.iconButton} aria-label="search"
                                            onClick={() => { console.log(this) }}>
                                            <SearchIcon />
                                        </IconButton>
                                    </div>
                                </Grid>
                            </div>
                        </Container>
                    </div>

                    <Container maxWidth="sm">
                        <div className={classes.filters} >
                            <FormControl className={classes.formControl}>
                                <InputLabel id="demo-mutiple-name-label">Category</InputLabel>
                                <Select
                                    labelId="demo-mutiple-name-label"
                                    id="demo-mutiple-name"
                                    value={bookFilters.category}
                                    onChange={(event) => {
                                        handleChangeFilter(event, "category");
                                    }}
                                    input={<Input />}
                                    MenuProps={MenuProps}
                                >
                                    {allFilters.category.map(category => (
                                        <MenuItem key={category} value={category} >
                                            {category}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <InputLabel id="demo-mutiple-name-label">Year</InputLabel>
                                <Select
                                    labelId="demo-mutiple-name-label"
                                    id="demo-mutiple-name"
                                    value={bookFilters.year}
                                    onChange={(event) => {
                                        handleChangeFilter(event, "year");
                                    }}
                                    input={<Input />}
                                    MenuProps={MenuProps}
                                >
                                    {allFilters.year.sort((a, b) => a - b)
                                        .map(year => (
                                            <MenuItem key={year} value={year} >
                                                {year}
                                            </MenuItem>
                                        ))}
                                </Select>
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <InputLabel id="demo-mutiple-name-label">Publisher</InputLabel>
                                <Select
                                    labelId="demo-mutiple-name-label"
                                    id="demo-mutiple-name"
                                    value={bookFilters.publisher}
                                    onChange={(event) => {
                                        handleChangeFilter(event, "publisher");
                                    }}
                                    input={<Input />}
                                    MenuProps={MenuProps}
                                >
                                    {allFilters.publisher.map(publisher => (
                                        <MenuItem key={publisher} value={publisher} >
                                            {publisher}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </div>
                    </Container>
                    <Container className={classes.cardGrid} maxWidth="md">
                        <Grid container spacing={4}>
                            {books.data
                                .filter((book) => searchTxt.toLowerCase() === '' || book.title.toLowerCase().includes(searchTxt))
                                .filter((book) => bookFilters.category === '' || book.category.includes(bookFilters.category))
                                .filter((book) => bookFilters.published === '' || book.published.substring(0, 4).includes(bookFilters.year))
                                .filter((book) => bookFilters.publisher === '' || book.publisher.includes(bookFilters.publisher))
                                .map(book => (
                                    <Grid item key={book.isbn} xs={6} sm={4} md={3}>
                                        <SingleProduct book={book} />
                                    </Grid>
                                ))}
                        </Grid>
                    </Container>
                </main>
                <Footer />
            </React.Fragment>
    );
}