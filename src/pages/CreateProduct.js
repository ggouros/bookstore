import React, { useState, useCallback, useContext } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import { withRouter } from "react-router-dom";

import { BooksContext } from '../contexts/books';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';


const useStyles = makeStyles(theme => ({
    bookGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    formButtonContainer: {
        textAlign: 'right',
    },
    formButton: {
        margin: '10px',
    }
}));

function Actions(props) {
    const classes = useStyles();

    return (
        <React.Fragment>
            <Button variant="outlined" color="default" className={classes.formButton}
                onClick={() => { props.handleReset() }}
            >
                Reset
            </Button>
            <Button variant="outlined" color="secondary" className={classes.formButton}
                onClick={() => { props.props.history.push(`/searchProduct`); }}
            >
                Cancel
            </Button>
            <Button type="submit" variant="outlined" color="primary" className={classes.formButton}>
                Save
            </Button>
        </React.Fragment >
    )
};

function CreateProduct(props) {
    const { addBook, books } = useContext(BooksContext);

    const resetFormObj = {
        "isbn": "",
        "title": "",
        "subtitle": "",
        "author": "",
        "published": "",
        "publisher": "",
        "rating": "",
        "pages": "",
        "description": "",
        "website": "",
        "category": [],
        "image": '',
    }
    const demoFormObj = {
        "isbn": "9781593275",
        "title": "New Book Added",
        "subtitle": "A Modern Introduction to Programming",
        "author": "Marijn Haverbeke",
        "published": "2014",
        "publisher": "No Starch Press",
        "rating": "5",
        "pages": 472,
        "description": "JavaScript lies at the heart of almost every modern web application, from social apps to the newest browser-based games. Though simple for beginners to pick up and play with, JavaScript is a flexible, complex language that you can use to build full-scale applications.",
        "website": "http://eloquentjavascript.net/",
        "category": [
            "Entertainment",
            "Science",
            "Comic"
        ],
        "image": '',
    }

    const [fields, setFields] = useState(resetFormObj);

    const handleFormChange = useCallback((event, fields) => {
        // Split categories to array
        if (event.target.id === "category") {
            const categArr = event.target.value.split(',');
            setFields({
                ...fields,
                [event.target.id]: categArr
            });
        } else {
            setFields({
                ...fields,
                [event.target.id]: event.target.value
            });
        }
    }, []);

    const handleSubmit = (event) => {
        console.log('A book was added: ', fields);
        addBook(fields);
        props.history.push(`/searchProduct`);
        event.preventDefault();
    }

    const handleReset = useCallback(() => {
        setFields(resetFormObj);
    }, []);

    const classes = useStyles();

    return (
        <React.Fragment>
            <Header />
            <main>
                <Container className={classes.bookGrid} maxWidth="md">
                    <form action="/" method="POST" onSubmit={(e) => { handleSubmit(e); }}>
                        <React.Fragment>
                            <Typography variant="h6" gutterBottom>
                                Add new Book
                            </Typography>
                            <Grid container spacing={3}>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        required
                                        id="title"
                                        name="title"
                                        label="Title"
                                        value={fields.title}
                                        fullWidth
                                        onChange={(event) => {
                                            handleFormChange(event, fields);
                                        }}
                                        inputProps={{ minLength: 10, maxLength: 120 }}
                                    // inputProps={{ minLength: 10, maxLength: 120, pattern: "^[A-Za-z0-9@”#&*!]" }}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        required
                                        id="image"
                                        name="Image"
                                        label="Image"
                                        type="file"
                                        fullWidth
                                        value={fields.image}
                                        onChange={(event) => {
                                            handleFormChange(event, fields);
                                        }}
                                        inputProps={{ accept: "image/x-png,image/gif,image/jpeg" }}
                                        helperText="Upload your image (png, gif, jpeg)"
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        required
                                        id="description"
                                        name="description"
                                        label="Description"
                                        value={fields.description}
                                        fullWidth
                                        multiline
                                        rows={3}
                                        onChange={(event) => {
                                            handleFormChange(event, fields);
                                        }}
                                        onInput={(e) => {
                                            e.target.value = e.target.value.charAt(0).toUpperCase() + e.target.value.slice(1);
                                        }}
                                        inputProps={{ maxLength: 120 }}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        required
                                        id="category"
                                        name="Category"
                                        label="Category"
                                        value={fields.category}
                                        fullWidth
                                        onChange={(event) => {
                                            handleFormChange(event, fields);
                                        }}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        required
                                        id="author"
                                        name="author"
                                        label="Author Name"
                                        value={fields.author}
                                        fullWidth
                                        onChange={(event) => {
                                            handleFormChange(event, fields);
                                        }}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField id="options"
                                        name="options"
                                        label="Options"
                                        value={fields.options}
                                        fullWidth
                                        onChange={(event) => {
                                            handleFormChange(event, fields);
                                        }}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        required
                                        id="publisher"
                                        name="publisher"
                                        label="Publisher"
                                        value={fields.publisher}
                                        fullWidth
                                        inputProps={{ minLength: 5, maxLength: 60 }}
                                        onChange={(event) => {
                                            handleFormChange(event, fields);
                                        }}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        required
                                        id="rating"
                                        name="rating"
                                        label="Rating"
                                        value={fields.rating}
                                        fullWidth
                                        onChange={(event) => {
                                            handleFormChange(event, fields);
                                        }}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        required
                                        id="published"
                                        name="Published"
                                        label="Published year"
                                        value={fields.published}
                                        type="number"
                                        fullWidth
                                        inputProps={{ min: "1000", max: "9999" }}
                                        onChange={(event) => {
                                            handleFormChange(event, fields);
                                        }}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        required
                                        id="isbn"
                                        name="Isbn"
                                        label="Isbn"
                                        value={fields.isbn}
                                        fullWidth
                                        inputProps={{ pattern: "[0-9]{1}[0-9]{9}" }}
                                        onChange={(event) => {
                                            handleFormChange(event, fields);
                                        }}
                                        helperText="10 digits"
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        required
                                        id="pages"
                                        name="Pages"
                                        label="Pages"
                                        value={fields.pages}
                                        type="number"
                                        fullWidth
                                        inputProps={{ max: "9999" }}
                                        onChange={(event) => {
                                            handleFormChange(event, fields);
                                        }}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControlLabel
                                        control={<Checkbox required color="secondary" name="saveAddress" value="yes" />}
                                        label="Concent to our terms and conditions"
                                    />
                                </Grid>
                            </Grid>
                        </React.Fragment>
                        <div className={classes.formButtonContainer}>
                            <Actions props={props} handleReset={() => handleReset()} />
                        </div>
                    </form>
                </Container>
            </main>
            <Footer />
        </React.Fragment>
    );
}

export default withRouter(CreateProduct)