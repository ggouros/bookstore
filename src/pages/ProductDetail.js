import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import { useParams, withRouter } from "react-router-dom";

import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';
import ProductRating from '../components/SingleProduct/ProductRating';
import RelatedProducts from '../components/RelatedProducts/RelatedProducts';

const useStyles = makeStyles(theme => ({
    bookGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    bookDetail: {
        paddingLeft: '50px !important',
    },
    bookAuthor: {
        display: 'flex',
        alignItems: 'baseline',
    },
    bookAuthorName: {
        paddingLeft: '10px',
    },
    bookActionBtn: {
        marginRight: theme.spacing(1),
        marginBottom: theme.spacing(1),
    }
}));


function ProductDetail(props) {
    const classes = useStyles();
    // const book = props.book;

    // We can use the `useParams` hook here to access
    // the dynamic pieces of the URL.
    const { bookIsbn } = useParams();
    // Extra code needed in case user lands directly here and booksData is empty
    const book = props.location.state.book;

    return (
        <React.Fragment>
            <Header />
            <main>
                <Container className={classes.bookGrid} maxWidth="md">
                    <Grid container spacing={2} justify="center">
                        <Grid item xs={12} sm={5} md={5}>
                            <img src={"https://source.unsplash.com/random/300x300"} alt="Logo" />
                            <div className={classes.bookAuthor}>
                                <Avatar>{book.author.charAt(0)}</Avatar>
                                <div className={classes.bookAuthorName}>
                                    {book.author}
                                </div>
                            </div>
                            <ProductRating />
                        </Grid>
                        <Grid item xs={12} sm={7} md={7} className={classes.bookDetail}>
                            <Typography component="h5" variant="h5" paragraph>
                                {book.title}
                            </Typography>
                            <Typography color="textSecondary" paragraph>
                                {book.description}
                            </Typography>
                            <Button variant="outlined" color="secondary" className={classes.bookActionBtn}>
                                Favorite
                            </Button>
                            <Button variant="outlined" color="primary" className={classes.bookActionBtn}>
                                Share
                            </Button>

                            <Typography color="textSecondary">
                                {`Category: ${book.category}`}
                            </Typography>
                            <Typography color="textSecondary">
                                {`Year: ${book.published.substring(0, 4)}`}
                            </Typography>
                            <Typography color="textSecondary" paragraph>
                                {`Number of Pages: ${book.pages}`}
                            </Typography>
                            <Typography color="textSecondary" paragraph>
                                {`Publisher: ${book.publisher}`}
                            </Typography>
                            <Typography color="textSecondary" paragraph>
                                {`ISBN: ${book.isbn}`}
                            </Typography>
                            <Button variant="contained" color="primary" size="large" fullWidth={true}>
                                BUY
                            </Button>
                        </Grid>
                    </Grid>
                </Container>
                <Container className={classes.bookGrid} maxWidth="md">
                    <RelatedProducts book={book} />
                </Container>
            </main>
            <Footer />
        </React.Fragment>
    );
}

export default withRouter(ProductDetail)