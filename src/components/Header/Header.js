import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import SearchIcon from '@material-ui/icons/Search';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { Link, withRouter } from "react-router-dom";
import { withTheme } from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
    toolbar: {
        borderBottom: `1px solid ${theme.palette.divider}`,
    },
    toolbarTitle: {
        flex: 1,
        cursor: 'pointer',
    },
    toolbarSecondary: {
        justifyContent: 'space-between',
        overflowX: 'auto',
    },
    toolbarLink: {
        padding: theme.spacing(1),
        flexShrink: 0,
    },
    headerButton: {
        color: 'white',
    }
}));

function Header(props) {
    const classes = useStyles();

    const title = 'Bookstore';
    const sections = [{ title: 'title', url: '/home' }];

    return (
        <React.Fragment>
            <AppBar position="relative">
                <Toolbar className={classes.toolbar}>
                    <Button
                        variant="outlined"
                        size="small"
                        className={classes.headerButton}
                        onClick={() => {
                            props.history.push('/createProduct');
                        }}>
                        <AddCircleOutlineIcon /> Create
                </Button>
                    <Typography
                        component="h2"
                        variant="h5"
                        color="inherit"
                        align="center"
                        noWrap
                        className={classes.toolbarTitle}
                        onClick={() => {
                            props.history.push('/');
                        }}
                    >
                        {title}
                    </Typography>
                    <Button
                        variant="outlined"
                        size="small"
                        className={classes.headerButton}
                        onClick={() => {
                            props.history.push('/searchProduct');
                        }}>
                        <SearchIcon /> Search
                </Button>
                </Toolbar>
            </AppBar>
        </React.Fragment>
    );
}

export default withRouter(Header)
