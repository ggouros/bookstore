import React, { useContext, useEffect } from 'react';
import { BooksContext } from '../../contexts/books';
import { useParams, withRouter } from "react-router-dom";
import SingleProduct from '../../components/SingleProduct/SingleProduct';
import Loading from '../../components/Loading/Loading';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Grid from '@material-ui/core/Grid';

export default function RelatedProducts(props) {

	var settings = {
		dots: true,
		infinite: false,
		speed: 500,
		slidesToShow: 4,
		slidesToScroll: 5
	};

	// We can use the `useParams` hook here to access
	// the dynamic pieces of the URL.
	const { bookIsbn } = useParams();

	// Extra code needed in case user lands directly here and booksData is empty
	const { getBooks, books } = useContext(BooksContext);

	const currentBook = props.book;

	if (!books.data) {
		getBooks();
	}

	return (
		!books.data || books.loading
			?
			<Loading />
			:
			<Slider {...settings}>
				{books.data
					.filter((book) => currentBook.category === '' || book.category.some(bookCategory => currentBook.category.includes(bookCategory)))
					.map((book) => {
						if (book.isbn != currentBook.isbn) {
							return (
								<Grid item key={book.isbn} md={11}>
									<SingleProduct book={book} key={book.isbn} />
								</Grid>
							)
						}
					})}
			</Slider>
	);
}
