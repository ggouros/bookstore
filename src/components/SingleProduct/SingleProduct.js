import React from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from "react-router-dom";

import ProductRating from './ProductRating';


const useStyles = makeStyles(theme => ({
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        // paddingTop: '56.25%', // 16:9
        paddingTop: '100%', // 1:1 Aspect Ratio
    },
    cardContent: {
        flexGrow: 1,
    },
}));

export default function SingleProduct(props) {
    const classes = useStyles();
    const book = props.book;

    return (
        <React.Fragment>
            <Card className={classes.card}>
                <Link to={{
                    pathname: `/productDetail/${book.isbn}`,
                    state: { book: book }
                }}>
                    <CardActionArea>
                        <CardMedia
                            className={classes.cardMedia}
                            image="https://source.unsplash.com/random/600x600"
                            title="Image title"
                        />
                        <CardContent className={classes.cardContent}>
                            <Typography align="center" gutterBottom variant="h5" component="h5">
                                {book.title}
                            </Typography>
                            {/* <Typography> */}
                            <ProductRating />
                            {/* </Typography> */}
                        </CardContent>
                        {/* <CardActions>
                    <Button size="small" color="primary">
                        View
                    </Button>
                    <Button size="small" color="primary">
                        Edit
                    </Button>
                </CardActions> */}
                    </CardActionArea>
                </Link>
            </Card>
        </React.Fragment >
    );
}