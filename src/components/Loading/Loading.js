import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles(theme => ({
    loadContainer: {
        display: 'flex',
        position: 'relative',
        flexGrow: 1,
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
        minHeight: '40px',
        padding: '40px',
    },
}));

export default function CircularIndeterminate() {
    const classes = useStyles();

    return (
        <div className={classes.loadContainer}>
            <CircularProgress />
        </div>
    );
}
