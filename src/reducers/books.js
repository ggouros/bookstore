import { useReducer } from 'react'
import { ADD_BOOK, GET_BOOKS } from '../constants'

const booksInitialState = {
    data: [],
    loading: true
};

const useBooksReducers = () => {
    const [books, dispatch] = useReducer((books, action) => {
        console.log('Reducer-books: ', books);
        console.log('Reducer-action: ', action);
        const { type, booksData, newBook } = action;

        switch (type) {
            case GET_BOOKS:
                if (books && books.data) {
                    return {
                        data: [
                            ...books.data,
                            ...booksData.data
                        ],
                        loading: booksData.loading
                    }
                } else {
                    return {
                        data: [
                            ...booksData.data
                        ],
                        loading: booksData.loading
                    }
                }
            case ADD_BOOK:
                if (books && books.data) {
                    return {
                        data: [
                            ...books.data,
                            newBook
                        ],
                        loading: false
                    }
                } else {
                    return {
                        data: [
                            newBook
                        ],
                        loading: false
                    }
                }
            default:
                return booksInitialState
        }
    }, [])

    return { books, dispatch }
}

export default useBooksReducers
