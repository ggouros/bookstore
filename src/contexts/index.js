import React from 'react'
import BookProvider from './books'
// import BaseProvider from './base'

const Provider = ({ children }) => (
    <BookProvider children={children} />
)

export default Provider