import React, { createContext } from 'react'
import bookActions from '../actions/books'
import useBookReducers from '../reducers/books'

const DEFAULTS = {
    books: []
}
const BooksContext = createContext(DEFAULTS)
const BookProvider = ({ children }) => {
    const { books, dispatch } = useBookReducers()

    return (
        <BooksContext.Provider value={{ ...bookActions(dispatch), books }}>
            {children}
        </BooksContext.Provider>
    )
}

export { BookProvider as default, BooksContext }