import axios from 'axios';
import { ADD_BOOK, GET_BOOKS } from '../constants'

export default dispatch => {
    const createRandomId = () =>
        Math.random()
            .toString(36)
            .substring(2)

    const addBook = newBook => {
        return dispatch({ type: ADD_BOOK, newBook })
    }

    const getBooks = async (ss) => {
        console.log("getBooks Action");
        let booksData = {
            data: [],
            loading: true
        };
        // Initial state
        // dispatch({ type: GET_BOOKS, booksData });

        await axios.get(
            "/books.json"
        )
            .then(function (response) {
                console.log("API response", response);
                booksData.data = response.data.books;
                booksData.loading = false;
            })
            .catch(function (error) {
                console.log(error);
                booksData.loading = false;
            });

        console.log('booksData after API response:', booksData);

        return dispatch({ type: GET_BOOKS, booksData })
    }

    return {
        addBook,
        getBooks
    }
}