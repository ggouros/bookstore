import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Provider from './contexts';

import HomePage from './pages/HomePage';
import SearchProduct from './pages/SearchProduct';
import ProductDetail from './pages/ProductDetail';
import CreateProduct from './pages/CreateProduct';

import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <Router>
      <Provider>
        <div>
          {/* <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/header">Header</Link>
            </li>
            <li>
              <Link to="/users">Users</Link>
            </li>
          </ul>
        </nav> */}

          {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
          <Switch>
            <Route path="/searchProduct">
              <SearchProduct />
            </Route>
            <Route path="/productDetail/:bookIsbn">
              <ProductDetail />
            </Route>
            <Route path="/createProduct">
              <CreateProduct />
            </Route>
            <Route path="/">
              <HomePage />
            </Route>
          </Switch>
        </div>
      </Provider>
    </Router>
  );
}

function Home() {
  return <h2>Home</h2>;
}

function About() {
  return <h2>About</h2>;
}

function Users() {
  return <h2>Users</h2>;
}


export default App;
